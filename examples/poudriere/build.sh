#!/usr/local/bin/bash

month=$(date +%m)
year=$(date +%Y)
month=$(( ${month#0} - 1 ))
quarter=$(( month / 3 + 1 ))
this_quarter="${year}Q$quarter"
echo "This quarter: $this_quarter"

month=$(( month - 3 ))
if [[ $month -lt 1 ]]; then
  month=$(( 12 + month ))
  year=$(( year - 1 ))
fi
quarter=$(( month / 3 + 1 ))
last_quarter="${year}Q$quarter"
echo "Last quarter: $last_quarter"

mkdir distfiles
poudriere ports -c -B "$last_quarter" -p "$last_quarter"
cat > /usr/local/etc/poudriere.d/default-make.conf << EOF
OPTIONS_UNSET+= DOCS NLS X11 EXAMPLES
EOF

cat > /usr/local/etc/poudriere.d/port-list << EOF
sysutils/screen
EOF

poudriere jail -c -j grfe -v "$(uname -r)"
poudriere bulk -f /usr/local/etc/poudriere.d/port-list \
               -j grfe -p "$last_quarter"
echo "$last_quarter" > distfiles/.quarter
