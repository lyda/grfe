#
# Makefile
# Kevin Lyda, 2022-02-16 20:07
#

files/usr/local/man/man8/grfe.8: grfe.md
	mkdir -p files/usr/local/man/man8
	pandoc --standalone --to man grfe.md > files/usr/local/man/man8/grfe.8

# vim:ft=make
#
