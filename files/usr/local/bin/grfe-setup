#!/usr/local/bin/bash

pkg_install_deps() {
  pkg install -y sudo git gitlab-runner ezjail flock jq py38-tomlkit
}

pf_setup() {
  # https://docs.freebsd.org/en/books/handbook/jails/#jails-ezjail
  sysrc cloned_interfaces="$1"
  service netif cloneup

  sysctl net.inet.ip.forwarding=1
  echo net.inet.ip.forwarding=1 >> /etc/sysctl.conf

  sysrc update_pf_conf="YES"
  cat > /etc/pf.conf.template << EOF
ext_if = INTERFACE

# NAT the traffic coming in via \$ext_if, only if the source isn't the IP bound
# to \$ext_if.
nat on \$ext_if inet from { $2.0/24 } to any -> \$ext_if

pass all
EOF
  service update_pf_conf start

  sysrc pf_enable="YES"
  service pf start
}

gitlab_runner_prep_setup() {
  sysrc gitlab_runner_prep_enable=YES
  sysrc gitlab_runner_prep_s3="$1"
  service gitlab_runner_prep start
}

ezjail_setup() {
  # Have ezjails use zfs
  sysrc -f /usr/local/etc/ezjail.conf ezjail_use_zfs=YES
  sysrc -f /usr/local/etc/ezjail.conf ezjail_use_zfs_for_jails=YES
  sysrc -f /usr/local/etc/ezjail.conf "ezjail_jailzfs=$1/ezjail"
  sysrc -f /usr/local/etc/ezjail.conf ezjail_default_flavour=grfe
  sysrc -f /usr/local/etc/ezjail.conf ezjail_default_execute=bash
  # Notes:
  #   ezjail_parameters is set due to info from this article:
  #     http://erdgeist.org/posts/2017/poudriere-in-ezjail/
  children_max=$(( $(sysctl -n hw.ncpu) * 2 ))
  sysrc -f /usr/local/etc/ezjail.conf ezjail_parameters="children.max=$children_max allow.mount=1 allow.mount.devfs=1 allow.mount.procfs=1 allow.mount.zfs=1 allow.mount.nullfs=1 allow.raw_sockets=1 allow.socket_af=1 allow.sysvipc=1 allow.chflags=1 enforce_statfs=1"
  sysrc ezjail_enable=YES
  service ezjail start
  ezjail-admin install
  cp -a /var/lib/grfe/flavours/grfe /usr/jails/flavours/grfe
}

grfe_setup() {
  sysrc -f /usr/local/etc/grfe.conf grfe_interface="$1"
  sysrc -f /usr/local/etc/grfe.conf grfe_subnet="$2"
  sysrc -f /usr/local/etc/grfe.conf grfe_zpool="$3"
}

gitlab_runner_setup() {
  sysrc gitlab_runner_enable=yes
  service gitlab_runner restart
  if [[ ! -f /var/tmp/gitlab_runner/.gitlab-runner/config.toml ]]; then
    echo "Warning: no runner configured."
    echo "Run 'service gitlab_runner register' to register a runner."
  fi
}

interface="$1"
subnet="$2"
bucket="$3"
zpool="$4"

if [[ -z "$interface" || -z "$subnet" || -z "$zpool" ]]; then
  echo "Usage: $0 <interface> <subnet> <bucket> <zpool>"
  echo "  If you don't want a bucket, just set it to ''"
  exit 1
fi

if [ -z "$(sysrc -n cloned_interfaces)" ]; then
  echo "Error: has this script already been run?"
fi
pkg_install_deps
pf_setup "$interface" "$subnet"
grfe_setup "$interface" "$subnet" "$zpool"
if [[ -n "$bucket" ]]; then
  gitlab_runner_prep_setup "$bucket"
fi
ezjail_setup "$zpool"
gitlab_runner_setup
