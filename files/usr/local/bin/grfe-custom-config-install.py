#!/usr/bin/env python3.8
"""TOML stuff."""

import shutil
import tomlkit

CUSTOM = {
    'config_exec': '/usr/local/libexec/grfe/config',
    'prepare_exec': '/usr/local/libexec/grfe/prepare',
    'run_exec': '/usr/local/libexec/grfe/run',
    'cleanup_exec': '/usr/local/libexec/grfe/cleanup',
    'graceful_kill_timeout': 200,
    'force_kill_timeout': 200
    }

def main():
  """main: stuff"""
  config_file = '/var/tmp/gitlab_runner/.gitlab-runner/config.toml'

  with open(config_file, 'r') as f:
    config = tomlkit.loads(''.join(f.readlines()))

  # Fix the configs.
  for runner in config['runners']:
    if runner['executor'] == 'custom':
      print(f'Configuring {runner["name"]} for grfe...')
      if not runner.get('custom', None):
        runner['custom'] = tomlkit.table()
        runner['custom'].indent(2)
      for key, val in CUSTOM.items():
        element = tomlkit.item({key: val})
        element.indent(2)
        runner['custom'].update(element)

  # Write it out.
  shutil.move(config_file, config_file + '.backup')
  toml_config = tomlkit.dumps(config)
  with open(config_file, 'w') as f:
    f.write(toml_config)

if __name__ == '__main__':
  main()
