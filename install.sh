#!/bin/sh

echo "Installing secret files."
for f in \
      usr/local/etc/sudoers.d/gitlab-runner \
      ; do
  mkdir -p "/$(dirname "$f")"
  install -m 600 files/$f /$f
done

echo "Installing config files."
for f in \
      var/lib/grfe/flavours/grfe/root/.gitconfig \
      var/lib/grfe/flavours/grfe/etc/rc.conf \
      etc/cron.d/grfe-grim-reaper \
      ; do
  mkdir -p "/$(dirname "$f")"
  install -m 644 files/$f /$f
done

echo "Installing scripts."
for f in \
      usr/local/bin/grfe-custom-config-install.py \
      usr/local/bin/grfe-grim-reaper \
      usr/local/bin/grfe-nuke-jails \
      usr/local/bin/grfe-setup \
      usr/local/etc/rc.d/gitlab_runner_prep \
      usr/local/etc/rc.d/update_pf_conf \
      usr/local/libexec/grfe/subr \
      usr/local/libexec/grfe/ip-reserve \
      usr/local/libexec/grfe/prepare \
      usr/local/libexec/grfe/services.d/zfs-clone.d/cleanup-pre \
      usr/local/libexec/grfe/services.d/zfs-clone.d/prepare-post-start \
      usr/local/libexec/grfe/services.d/poudriere.d/cleanup-pre \
      usr/local/libexec/grfe/services.d/poudriere.d/cleanup-post \
      usr/local/libexec/grfe/services.d/poudriere.d/prepare-post-start \
      usr/local/libexec/grfe/services.d/poudriere.d/run-post-get_sources \
      usr/local/libexec/grfe/services.d/poudriere-git.d/cleanup-pre \
      usr/local/libexec/grfe/services.d/poudriere-git.d/cleanup-post \
      usr/local/libexec/grfe/services.d/poudriere-git.d/prepare-post-start \
      usr/local/libexec/grfe/services.d/poudriere-git.d/run-post-get_sources \
      usr/local/libexec/grfe/services.d/nullfs.d/cleanup-pre \
      usr/local/libexec/grfe/services.d/nullfs.d/prepare-pre-start \
      usr/local/libexec/grfe/config \
      usr/local/libexec/grfe/run \
      usr/local/libexec/grfe/cleanup \
      usr/local/man/man8/grfe.8 \
      var/lib/grfe/flavours/grfe/etc/rc.d/grfe_flavour_setup \
      ; do
  mkdir -p "/$(dirname "$f")"
  install files/$f /$f
done
