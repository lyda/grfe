% GRFE(8) Gitlab Runner FreeBSD Custom Executor

# NAME

grfe - Gitlab Runner FreeBSD Custom Executor

# SYNOPSIS

This custom executor allows build jobs to be run inside of jails, allows
for services to be provided and allows for caches to be used to speed up
builds.  This enables gitlab builds to happen in controlled environments.

# DESCRIPTION

The grfe(8) system is a custom executor for gitlab runner that creates
jails to run jobs in. Once done, the jail is destroyed.

It uses ezjail(7) to manage the jails.  In a future release it would be
good to remove this dependency.

There's an assumption throughout this that the gitlab-runner user has
full sudo access - this is needed to create/manage/destroy the jails.
In addition, some scripts like the grfe-grim-reaper script need to be
able to poweroff the machine due to inactivity.

Networking changes are needed to allow jails to have access to
the internet.  They use this to communicate with the gitlab server,
to update packages and so on.

# CONFIGURATION

There are two main things that need to be configured.  The gitlab-runner
toml configuration file needs to be changed once registration is complete.
In addition, the grfe configuration need a configuration file.

## gitlab-runner registration and toml file

Follow the gitlab runner documentation to register the
runner.  This is done with the `register` command for the
gitlab runner init script.  It will generate a config file in
`/var/tmp/gitlab_runner/.gitlab-runner/config.toml`.

Once registration is complete, the config file needs to be
configured to use grfe.  You can run
`grfe-custom-config-install.py` to do this, or you can make the changes
manually (the `runners.custom` section should already exist):

```
  [runners.custom]
    config_exec = "/usr/local/libexec/grfe/config"
    prepare_exec = "/usr/local/libexec/grfe/prepare"
    run_exec = "/usr/local/libexec/grfe/run"
    cleanup_exec = "/usr/local/libexec/grfe/cleanup"
    graceful_kill_timeout = 200
    force_kill_timeout = 200
```

It's best to `restart` the gitlab runner daemon at this point.

## grfe configuration

Configuration for grfe is done via `/usr/local/etc/grfe.conf`.  However a
number of other subsystems need to be configured for grfe to work.  These
include pf(4), ezjail(1) and zfs(8). To help with this, there's a script
to do the setup for a more vanilla system.  The description of what the
script does will cover the subsytems changed.

The script, `/usr/local/bin/grfe-setup`, will make system changes to
enable jails to communicate externally and configure grfe to make jails
and use zfs based on what's there.  It takes the following arguments:

  * interface - The network interface used by the jails (usually lo1).
  * subnet - The 24 bit network for the jails.
  * bucket - The bucket path to save the gitlab-runner config to.
  * zpool - this is the zfs pool to be used by the grfe jails.

### Network config (interface and subnet)

It's best to review the `/usr/local/bin/grfe-setup` script to see the
exact configuration steps, but roughly the loopback nic is cloned,
the kernel is configured to do ip forwarding and pf(4) is configured
(pf.conf(5)) to handle network address translation.

### Config saving (bucket)

Example: `s3://your-gitlab-bucket/gitlab-runner-config`

The `gitlab_runner_prep_setup` startup script will, if the
`gitlab_runner_prep_enable` rc.conf(5) variable is set, restore and
backup the gitlab-runner(1) config file to the s3 bucket path identified by
the `gitlab_runner_prep_s3` rc.conf(5) variable.  This is useful in
cases where you might want to completely destroy the build machine and
recreate it as needed.  Just make sure that when the machine is created
that these variables are configured.

The configs will be stored in this path:
`${gitlab_runner_prep_s3}/$(hostname -s)/config.toml` .
This will allow you to use the same path with multiple machines.

### Jail config (zpool)

It's best to review the `/usr/local/bin/grfe-setup` script to see the
exact configuration steps, but ezjail is configured with the given
zpool and the grfe "flavour" in /var/lib/grfe/flavours/grfe is copied
to /usr/jails/flavours/grfe.  In addition ezjails is configured to use
a cloned loopback and the subnet that will be NATed.

# SERVICES

Services are run outside the jail to configure things for the build.
These can do a number of things.  They can mount filesystems into the
jail, they can configure more complex software.  They could start and
clean up other jails to provide services.

The following services are provided: `nullfs`, `zfs-clone`,
`poudriere` and `poudriere-git`.

## nullfs service

Arguments: [`directory to be mounted`] [`where to mount it in the jail`]

Example:

```
  services:
    - 'nullfs:/var/packages:/var/packages'
```

This will mount a directory on the host machine via `nullfs(5)` into
the jail.

## zfs-clone service

Arguments: [`snapshot to be cloned`] [`where to mount it in the jail`]

Example:

```
  services:
    - 'zfs-clone:FreeBSD-13.0-RELEASE:/var/cdroms/image'
```

This will add a rw clone of a zfs snapshot into a jail.

## poudriere service

Arguments: none

Example:

```
  services:
    - 'poudriere'
```

This will prep the jail to run poudriere.

## poudriere-git service

Arguments: none

Example:

```
  services:
    - 'poudriere-git'
```

This will prep the jail to run poudriere.  Poudriere will be installed from source.

# FILES

## /usr/local/etc/grfe.conf

Configuration file for grfe.

## /tmp/no-cleanup

Skip the cleanup scripts.  It allows jails to stick around.

# SEE ALSO

Gitlab Runner documentation is at <https://docs.gitlab.com/runner/>.
The documentation for the custom executor is at
<https://docs.gitlab.com/runner/executors/custom.html>.

# AUTHOR

Written by Kevin Lyda <lyda@titanhq.com>.

# BUGS

Probably.  This is making a number of changes to a number of systems and
the tools only provide so much feedback on failures.  I suspect there
are lots of edge cases which need to be explored.
